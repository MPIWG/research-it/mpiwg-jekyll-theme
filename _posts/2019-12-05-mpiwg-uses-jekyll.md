---
layout: post
title: "The MPIWG uses Jekyll"
excerpt_separator: <!--more-->
date: 2020-07-01
category: announcements
author:
  - name: Florian Kräutli
    url: http://www.mpiwg-berlin.mpg.de/users/fkraeutli
---

For creating simple websites for projects or small databases, the MPIWG now has its own template for Jekyll based microsites.

<!--more-->

This adds to the options that the MPIWG has available for digital publication.