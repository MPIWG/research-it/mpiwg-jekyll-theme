---
layout: default
title: About
subtitle: Learn more about this Jekyll template
permalink: /about
titleimage: https://www.mpiwg-berlin.mpg.de/sites/default/files/2017-11/20170601-architecture-mainb_82770150dpisrgb.jpg
---

### A subtitle

{% accordeon An Accordeon %}

The body of the accordeon

{% endaccordeon %}

{% accordeon Another Accordeon %}

The body of the accordeon

{% endaccordeon %}