---
title: People
layout: people
permalink: /people/
titleimage: https://www.mpiwg-berlin.mpg.de/sites/default/files/2017-11/20170601-architecture-mainb_82842150dpisrgb.jpg
---

{% intro

A list of people involved in the project

%}