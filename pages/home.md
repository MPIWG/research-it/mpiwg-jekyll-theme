---
layout: default
title: Home
permalink: /
titleimage: https://www.mpiwg-berlin.mpg.de/sites/default/files/2017-11/20170601-architecture-mainb_82770150dpisrgb.jpg
---

{% intro 

Founded in 1994, the Max Planck Institute for the History of Science (MPIWG) in Berlin is one of the more than 80 research Institutes administered by the Max Planck Society in the sciences and humanities. 

%}

The Institute comprises three departments under the direction of Jürgen Renn (I), Lorraine Daston (II), and Dagmar Schäfer (III). 
Hans-Jörg Rheinberger, who headed Department III from 1995 to 2014, remains at the MPIWG as emeritus. The three directors administer the Institute collectively; the position of Executive Director rotates every two to three years. Dagmar Schäfer has served as Executive Director since July 1, 2019.

![Alternative text for the image](assets/img/mpiwgLogo.png "The title of the image, which will be displayed on hover")
*A caption for the image, wrapped in asteriks*

Besides the research departments, there are four Research Groups (as of December  2017), each directed by one junior Research Group Leader. Various institutions, such as the Max Planck Society and the German Research Fund (DFG) fund them. They are independent in their research programs.

Since its inception, the Institute has approached the fundamental questions of the history of knowledge from the Neolithic era to the present day. Researchers at the Institute pursue a historical epistemology in their study of how new categories of thought, proof, and experience have emerged in interactions between the sciences and their ambient cultures.