# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = "mpiwg-jekyll-theme"
  spec.version       = "0.6.5"
  spec.authors       = ["Florian Kräutli"]
  spec.email         = ["fkraeutli@mpiwg-berlin.mpg.de"]

  spec.summary       = "A MPIWG Microsite Jekyll theme."
  spec.homepage      = "http://www.mpiwg-berlin.mpg.de"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").select { |f| f.match(%r!^(assets|_layouts|_includes|_sass|LICENSE|README)!i) }

  spec.add_runtime_dependency "jekyll", "~> 4.0"

  spec.add_development_dependency "bundler", "~> 2.0"
  spec.add_development_dependency "rake", "~> 12.0"
end
