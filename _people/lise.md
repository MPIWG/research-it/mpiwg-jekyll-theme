---
firstname:  Lise
lastname: Meitner
image: https://upload.wikimedia.org/wikipedia/commons/thumb/0/03/Lise_Meitner_%281878-1968%29%2C_lecturing_at_Catholic_University%2C_Washington%2C_D.C.%2C_1946.jpg/1280px-Lise_Meitner_%281878-1968%29%2C_lecturing_at_Catholic_University%2C_Washington%2C_D.C.%2C_1946.jpg
profile: https://en.wikipedia.org/wiki/Lise_Meitner
position: Physicist
order: 010
---
Lise Meitner was an Austrian-Swedish physicist who worked on radioactivity and nuclear physics. Meitner, Otto Hahn and Otto Robert Frisch led the small group of scientists who first discovered nuclear fission of uranium when it absorbed an extra neutron; the results were published in early 1939.Meitner, Hahn and Frisch understood that the fission process, which splits the atomic nucleus of uranium into two smaller nuclei, must be accompanied by an enormous release of energy. Their research into nuclear fission helped to pioneer nuclear reactors to generate electricity as well as the development of nuclear weapons during World War II.