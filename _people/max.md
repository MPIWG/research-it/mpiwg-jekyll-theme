---
firstname:  Max
lastname: Planck
image: https://upload.wikimedia.org/wikipedia/commons/thumb/c/c7/Max_Planck_1933.jpg/966px-Max_Planck_1933.jpg
profile: https://en.wikipedia.org/wiki/Max_Planck
position: Theoretical Physicist
order: 020
---
Max Planck was a theoretical physicist. In 1948, the German scientific institution the Kaiser Wilhelm Society (of which Planck was twice president) was renamed the Max Planck Society (MPS). The MPS now includes 83 institutions representing a wide range of scientific directions.